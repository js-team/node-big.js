# Installation
> `npm install --save @types/big.js`

# Summary
This package contains type definitions for big.js (https://github.com/MikeMcl/big.js/).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/big.js.

### Additional Details
 * Last updated: Tue, 05 Jul 2022 23:32:28 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by [Steve Ognibene](https://github.com/nycdotnet), and [Roman Nuritdinov (Ky6uk)](https://github.com/Ky6uk).
